FROM python:3.8.0-alpine

RUN pip install pipenv

WORKDIR /root/api

COPY Pipfile.lock Pipfile ./

RUN pipenv install --dev

EXPOSE 5000
