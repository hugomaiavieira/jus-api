# Jusbrasil (api)

This is the project that contains the API and its queue service for crawling the data from the courts websites.

## Setup, run the apps and tests

Take a look at: https://gitlab.com/hugomaiavieira/jus

## TODO

- Deploy on heroku
- We should have a DTO object to be used as interface for the messages in the queue. This way we would isolate the messages from our bussiness code. Also the messages could have some extra metadata like event name, timestamp, and version.
- Write some integration/end-to-end tests?

## Decisions

- I've decided to save in Redis and use a temp key because it doesn't seem to be an information that requires to be available for a long time.
