import redis, os

__redis_url = os.environ.get('REDIS_URL')
if not __redis_url:
    raise Exception(
        'You must set the REDIS_URL env var following '
        'the format redis://hostname:port/db_number'
    )

pool = redis.from_url(__redis_url)
