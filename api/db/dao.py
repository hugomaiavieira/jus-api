from datetime import timedelta
from json import dumps, loads
from api.db import pool
from api.models.lawsuit_search import LawsuitSearch

class LawsuitSearchDAO:
    '''
    Provides specific operations for the LawsuitSearch to access the
    persistence layer.
    '''
    def set(self, obj, minutes=5):
        value = dumps({'number': obj.number, 'result': obj.result})
        pool.setex(self.__db_key(obj.number), timedelta(minutes=minutes), value)

    def get_by_number(self, number):
        record = pool.get(self.__db_key(number))
        if not record: return None

        record = loads(record)
        return LawsuitSearch(record['number'], record['result'])

    @staticmethod
    def __db_key(number):
        db_namespace = 'lawsuit_search'

        return f'{db_namespace}:{number}'
