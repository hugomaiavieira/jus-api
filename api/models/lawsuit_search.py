from json import dumps, loads
import re

class LawsuitSearch:
    def __init__(self, number, result=None):
        self.__number = self.__normalize_number(number)
        self.__result = result

    def update_result(self, data):
        if type(data) == dict:
            self.__result = data
            return self.save()
        else:
            return False

    def save(self):
        if not self.is_valid(): return False

        LawsuitSearchDAO().set(self)
        return True

    def is_valid(self):
        return len(self.number) == 20

    def to_json(self):
        return dumps({
            'number': self.number,
            'status': self.status,
            'result': self.result,
        })

    @property
    def number(self):
        return self.__number

    @property
    def result(self):
        return self.__result

    @property
    def status(self):
        return 'in_progress' if self.result == None else 'done'

    @staticmethod
    def __normalize_number(number):
        return re.sub(r'[^\d]', '', str(number))

    @classmethod
    def get_by_number(cls, number):
        return LawsuitSearchDAO().get_by_number(number)

from api.db.dao import LawsuitSearchDAO
