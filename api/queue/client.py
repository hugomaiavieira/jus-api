import pika, os
from os import environ
from urllib.parse import urlparse

RESULT_QUEUE_NAME = 'jusbrasil/searchResult'
INPUT_QUEUE_NAME = 'jusbrasil/searchInput'

class Client:
    '''Client for the queue system (Rabbitmq).'''
    def __init__(self):
        url_str = environ.get('CLOUDAMQP_URL')
        if not url_str:
            raise Exception(
                'you must set the CLOUDAMQP_URL env var following'
                'the format amqp://user:pass@hostname//'
            )

        url = urlparse(url_str)

        self.connection_params = pika.ConnectionParameters(
            host=url.hostname,
            virtual_host=url.path[1:],
            credentials=pika.PlainCredentials(url.username, url.password)
        )

    def publish(self, message):
        connection = pika.BlockingConnection(self.connection_params)
        channel = connection.channel()
        channel.queue_declare(queue=INPUT_QUEUE_NAME, durable=True)

        channel.basic_publish(
            exchange='',
            routing_key=INPUT_QUEUE_NAME,
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2 # make message persistent
            )
        )
        print(" [x] Sent %r" % message)
        connection.close()

    def consume(self, on_message_callback):
        connection = pika.BlockingConnection(self.connection_params)
        channel = connection.channel()

        channel.queue_declare(queue=RESULT_QUEUE_NAME, durable=True)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(
            queue=RESULT_QUEUE_NAME,
            on_message_callback=on_message_callback
        )

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()
