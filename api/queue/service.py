from json import loads
from api.queue.client import Client
from api.models import lawsuit_search

def consume_callback(channel, deliver, _properties, body):
    print(' [x] Message received')
    message = loads(body, encoding='utf-8')

    if message.get('number'):
        if ls := lawsuit_search.LawsuitSearch.get_by_number(message['number']):
            ls.update_result(message)
        else:
            lawsuit_search.LawsuitSearch(message['number'], message).save()
        print(' [x] Message processed')
    else:
        print(' [x] Error: Message do not have a number')

    channel.basic_ack(delivery_tag=deliver.delivery_tag)
