from flask import request, jsonify
from api import app
from api.queue import client
from api.models.lawsuit_search import LawsuitSearch

@app.route('/lawsuit/search', methods=['POST'])
def lawsuit_search():
    if not request.json: return jsonify(error='no body'), 400

    ls = LawsuitSearch(request.json['number'])

    if ls.save():
        qclient = client.Client()
        qclient.publish(ls.to_json())

        return app.response_class(
            response=ls.to_json(),
            status=202,
            mimetype='application/json'
        )
    else:
        return jsonify(error='invalid number'), 422

@app.route('/lawsuit/search/<string:number>', methods=['GET'])
def lawsuit_search_result(number):
    if ls := LawsuitSearch.get_by_number(number):
        return app.response_class(
            response=ls.to_json(),
            status=200,
            mimetype='application/json'
        )
    else:
        return jsonify(error='not found'), 404
