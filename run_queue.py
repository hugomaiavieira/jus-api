from api.queue.client import Client
from api.queue.service import consume_callback

if __name__ == "__main__":
    try:
        Client().consume(consume_callback)
    except KeyboardInterrupt:
        raise SystemExit()
