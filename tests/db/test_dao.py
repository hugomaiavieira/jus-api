from api.db.dao import LawsuitSearchDAO
from api.models.lawsuit_search import LawsuitSearch
from datetime import timedelta

def test_set_save_the_object_data_as_json_string_in_the_database_for_five_minutes(mocker):
    setex_mock = mocker.patch('api.db.pool.setex', autospec=True)
    ls = LawsuitSearch('01234567890123456789', {'court': 'tjms'})

    LawsuitSearchDAO().set(ls)

    setex_mock.assert_called_with(
        'lawsuit_search:01234567890123456789',
        timedelta(minutes=5),
        '{"number": "01234567890123456789", "result": {"court": "tjms"}}'
    )

def test_get_by_number_returns_a_LawsuitSearch_instance_when_record_exists_in_the_database(mocker):
    json = '{"number": "01234567890123456789", "result": {"court": "tjms"}}'
    get_mock = mocker.patch('api.db.pool.get', autospec=True, return_value=json)

    ls = LawsuitSearchDAO().get_by_number('01234567890123456789')

    assert ls.__class__ == LawsuitSearch
    assert ls.number == '01234567890123456789'
    assert ls.result == {'court': 'tjms'}
    get_mock.assert_called_with('lawsuit_search:01234567890123456789')

def test_get_by_number_returns_None_when_record_does_not_exist_in_the_database(mocker):
    get_mock = mocker.patch('api.db.pool.get', autospec=True, return_value=None)

    ls = LawsuitSearchDAO().get_by_number('01234567890123456789')

    assert ls == None
    get_mock.assert_called_with('lawsuit_search:01234567890123456789')
