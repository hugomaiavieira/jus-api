from api.models.lawsuit_search import LawsuitSearch

def test_normalize_the_given_number_when_initializing():
    lawsuit_search = LawsuitSearch(' 12345678901234567890a')
    assert lawsuit_search.number == '12345678901234567890'

def test_is_valid_when_number_has_20_digits():
    lawsuit_search = LawsuitSearch('12345678901234567890')
    assert lawsuit_search.is_valid() == True

def test_is_invalid_when_number_has_more_than_20_digits():
    lawsuit_search = LawsuitSearch('123456789012345678901')
    assert lawsuit_search.is_valid() == False

def test_is_invalid_when_number_has_less_than_20_digits():
    lawsuit_search = LawsuitSearch('1234567890123456789')
    assert lawsuit_search.is_valid() == False

def test_status_is_done_when_there_is_a_result():
    lawsuit_search = LawsuitSearch('42', {'court': 'tjms'})
    assert lawsuit_search.status == 'done'

def test_status_is_in_progress_when_there_is_no_result():
    lawsuit_search = LawsuitSearch('42')
    assert lawsuit_search.status == 'in_progress'

def test_represents_its_data_as_json():
    lawsuit_search = LawsuitSearch('42')
    expected = '{"number": "42", "status": "in_progress", "result": null}'
    assert lawsuit_search.to_json() == expected

    lawsuit_search = LawsuitSearch('42', {'court': 'tjms'})
    expected = '{"number": "42", "status": "done", "result": {"court": "tjms"}}'
    assert lawsuit_search.to_json() == expected

def test_save_when_valid_sets_it_to_database(mocker):
    doa_set_mock = mocker.patch('api.db.dao.LawsuitSearchDAO.set')
    lawsuit_search = LawsuitSearch('12345678901234567890')

    assert lawsuit_search.save() == True
    doa_set_mock.assert_called_with(lawsuit_search)

def test_save_when_invalid_not_set_it_to_database(mocker):
    doa_set_mock = mocker.patch('api.db.dao.LawsuitSearchDAO.set')
    lawsuit_search = LawsuitSearch('invalid-number')

    assert lawsuit_search.save() == False
    doa_set_mock.assert_not_called()

def test_update_result_when_valid_sets_it_to_database(mocker):
    doa_set_mock = mocker.patch('api.db.dao.LawsuitSearchDAO.set')
    lawsuit_search = LawsuitSearch('12345678901234567890')

    assert lawsuit_search.update_result({'court': 'tjal'}) == True
    doa_set_mock.assert_called_with(lawsuit_search)

def test_update_result_when_invalid_not_set_it_to_database(mocker):
    doa_set_mock = mocker.patch('api.db.dao.LawsuitSearchDAO.set')
    lawsuit_search = LawsuitSearch('12345678901234567890')

    assert lawsuit_search.update_result("{'court': 'tjal'}") == False
    doa_set_mock.assert_not_called()

def test_get_by_number_returns_an_instance_with_data_from_database(mocker):
    return_obj = LawsuitSearch('42')
    doa_get_by_number_mock = mocker.patch(
            'api.db.dao.LawsuitSearchDAO.get_by_number',
            return_value=return_obj
        )

    assert LawsuitSearch.get_by_number('42') == return_obj
    doa_get_by_number_mock.assert_called_with('42')
