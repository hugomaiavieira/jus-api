from api.queue.service import consume_callback

def test_consume_callback_when_message_does_not_have_a_number_only_do_the_acknowledge(mocker, capsys):
    channel_mock = mocker.Mock()
    deliver_mock = mocker.Mock(delivery_tag=1)
    body = '{}'

    consume_callback(channel_mock, deliver_mock, mocker.ANY, body)

    captured = capsys.readouterr()
    assert captured.out == ' [x] Message received\n [x] Error: Message do not have a number\n'
    channel_mock.basic_ack.assert_called_with(delivery_tag=1)

def test_consume_callback_when_the_search_does_not_exist_anymore_save_again_with_result_and_do_the_acknowledge(mocker, capsys):
    LawsuitSearchMock = mocker.patch(
        'api.models.lawsuit_search.LawsuitSearch',
        get_by_number=mocker.Mock(return_value=None)
    )
    channel_mock = mocker.Mock()
    deliver_mock = mocker.Mock(delivery_tag=1)
    body = '{"number": "01234567890123456789", "court": "tjal"}'

    consume_callback(channel_mock, deliver_mock, mocker.ANY, body)

    captured = capsys.readouterr()
    assert captured.out == ' [x] Message received\n [x] Message processed\n'
    LawsuitSearchMock.assert_called_with(
        '01234567890123456789',
        {'number': '01234567890123456789', 'court': 'tjal'}
    )
    LawsuitSearchMock.return_value.save.assert_called()
    channel_mock.basic_ack.assert_called_with(delivery_tag=1)

def test_consume_callback_when_search_exists_update_the_result_and_do_the_acknowledge(mocker, capsys):
    channel_mock = mocker.Mock()
    deliver_mock = mocker.Mock(delivery_tag=1)
    LawsuitSearchMock = mocker.patch(
        'api.models.lawsuit_search.LawsuitSearch',
        get_by_number=mocker.Mock(name='LawsuitSearch instance')
    )
    ls_instance = LawsuitSearchMock.get_by_number.return_value
    body = '{"number": "01234567890123456789", "court": "tjal"}'

    consume_callback(channel_mock, deliver_mock, mocker.ANY, body)

    captured = capsys.readouterr()
    assert captured.out == ' [x] Message received\n [x] Message processed\n'
    ls_instance.update_result.assert_called_with(
        {'number': '01234567890123456789', 'court': 'tjal'}
    )
    channel_mock.basic_ack.assert_called_with(delivery_tag=1)
