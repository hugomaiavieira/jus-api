import pytest
from api import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    return app.test_client()

def test_post_search_without_body_returns_400_status_code(client):
    response = client.post('/lawsuit/search')

    assert response.status_code == 400
    assert response.get_json() == {'error': 'no body'}

def test_post_search_with_invalid_lawsuit_number_returns_422_status_code_and_error_description_as_json(client):
    response = client.post('/lawsuit/search', json={'number': '123'})

    assert response.status_code == 422
    assert response.get_json() == {'error': 'invalid number'}

def test_post_search_with_valid_lawsuit_number_publishes_the_search_in_the_queue_returns_the_created_entity_as_json(client, mocker):
    mocker.patch(
        'api.models.lawsuit_search.LawsuitSearch.save',
        return_value=True
    )
    qclient_mock = mocker.patch('api.queue.client.Client').return_value

    response = client.post(
        '/lawsuit/search',
        json={'number': '01234567890123456789'}
    )

    qclient_mock.publish.assert_called_with(
        '{"number": "01234567890123456789", "status": "in_progress", "result": null}'
    )
    assert response.status_code == 202
    assert response.get_json() == {
        'number': '01234567890123456789',
        'status': 'in_progress',
        'result': None
    }

def test_get_search_by_not_existent_lawsuit_number_returns_404_status_code_and_error_description_as_json(client, mocker):
    mocker.patch(
        'api.models.lawsuit_search.LawsuitSearch.get_by_number',
        return_value=None
    )

    response = client.get('/lawsuit/search/00000000000000000000')

    assert response.status_code == 404
    assert response.get_json() == {'error': 'not found'}

def test_get_search_by_existent_lawsuit_number_returns_200_status_code_and_the_search_entity_json_representation(client, mocker):
    ls = mocker.Mock(
        name='LawsuitSearch',
        to_json=mocker.Mock(return_value='{"number": "123"}')
    )
    mocker.patch(
        'api.models.lawsuit_search.LawsuitSearch.get_by_number',
        return_value=ls
    )

    response = client.get('/lawsuit/search/01234567890123456789')

    assert response.status_code == 200
    assert response.get_json() == {'number': '123'}
